﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Give us access to unitys built in UI tools
using UnityEngine.UI;
public class ShowUI : MonoBehaviour
{
    public Canvas overlayScreen;
    


    void OnTriggerEnter(Collider TheThingThatWalkedIntoMe)
    {
        if (TheThingThatWalkedIntoMe.tag == "Player")
        {
            //hide the UI canvas
            overlayScreen.enabled = true;
            Debug.Log("You Entered the area");
        }
    }

    void OnTriggerExit(Collider TheThingThatWalkedIntoMe)
    {
        if(TheThingThatWalkedIntoMe.tag == "Player")
        {
            //hide the UI canvas
            overlayScreen.enabled = false;
            Debug.Log("You left the area");

        }
    }
}
